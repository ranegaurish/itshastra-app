angular.module('app.routes', [])

    .config(function ($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js

        $stateProvider

        //homepage
            .state('menu.home', {
                url: '/home',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/home_template/home.html',
                        controller: 'homeCtrl'
                    }
                }
            })

            //dashboard page
            .state('menu.dashboard', {
                url: '/dashboard',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/dashboard.html',
                        controller: 'dashboardCtrl'
                    }
                }
            })

            //home page
            .state('menu.login', {
                url: '/login',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/login.html',
                        controller: 'loginCtrl'
                    }
                }
            })

            //registration
            .state('menu.registration', {
                url: '/registration',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/home_template/registration.html',
                        controller: 'registrationCtrl'
                    }
                }
            })

            //customer details
            .state('menu.customerdetails', {
                url: '/customer-details/:id',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/customer_detail.html',
                        controller: 'customerDetailsCtrl'
                    }
                }
            })


            //appointment create
            .state('menu.appointment', {
                url: '/appointment',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/components/pages/appointment.html',
                        controller: 'appointmentCreateCtl'
                    }
                }
            })

            //allocation
            .state('menu.allocation', {
                url: '/allocation',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/components/pages/allocation.html',
                        controller: 'allocationCreateCtl'
                    }
                }
            })

            //location
            .state('menu.location', {
                url: '/location',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/home_template/location.html',
                        controller: 'locationCtrl'
                    }
                }
            })

            //contact
            .state('menu.contact', {
                url: '/contact',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/home_template/contact.html',
                        controller: 'contactCtrl'
                    }
                }
            })

            .state('menu.scassisment', {
                url: '/scassisment',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/home_template/home.html',
                        controller: 'sc_assissmentCtrl'
                    }
                }
            })


            //menu function
            .state('menu', {
                url: '/side-menu21',
                templateUrl: 'templates/menu.html',
                cache: false,
                controller: 'menuCtrl',
                abstract: true
            })

            .state('connectionLost', {
                url: '/connection-lost',
                templateUrl: 'templates/connection_lost.html',
                controller: 'connectionCtrl',
                cache: false,
            })

            .state('test', {
                url: '/test/:id',
                controller: 'testCtrl',
                templateUrl: function ($stateParams) {
                    console.debug('i have got the base', $stateParams.id);
                    return 'templates/test2.html';
                },
                cache: false,
                resolve: {
                    somedata: function ($log) {
                        var init = {};
                        init.fun = function (s) {
                            return 'instesd the sunction';
                        };
                        return init;
                    },
                }
            })

        $urlRouterProvider.otherwise('/side-menu21/home')

    });