angular.module('app.form_routes', [])

    .config(function ($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js

        $stateProvider


        /* S&C Route
         * ***********************************************************************************
         */

            .state('menu.snc_consultation', {
                url: '/snc/consultation',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/forms/snc/consultation.html',
                        controller: 'sncConsultationCtrl'
                    }
                }
            })

            .state('menu.snc_assessment', {
                url: '/snc/assessment',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/forms/snc/assessment.html',
                        controller: 'sncAssessmentCtrl'
                    }
                }
            })

            .state('menu.snc_feedback', {
                url: '/snc/feedback',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/forms/snc/feedback.html',
                        controller: 'sncFeedbackCtrl'
                    }
                }
            })

            /* Physio therapy route
             * ***********************************************************************************
             */

            .state('menu.physiotherpay_consultation', {
                url: '/physiotherpay/consultation',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/forms/physiotherpay/consultation.html',
                        controller: 'physiotherpayConsultationCtrl'
                    }
                }
            })

            .state('menu.physiotherpay_assessment', {


                url: '/physiotherpay/assessment',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/forms/physiotherpay/assessement.html',
                        controller: 'physioAssessmentCtrl'
                    }
                }
            })

            .state('menu.physiotherpay_feedback', {
                url: '/physiotherpay/feedback',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/forms/physiotherpay/feedback.html',
                        controller: 'physiotherpayFeedbackCtrl'
                    }
                }
            })
            .state('menu.physio_comment', {
                url: '/physio/comment',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/forms/physiotherpay/comment.html',
                        controller: 'physioCommentCtrl'
                    }
                }
            })

            /* Message-Therapy Route
             * ***********************************************************************************
             */

            .state('menu.messagetherapy_consultation', {
                url: '/messagetherapy/consultation',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/forms/message/consultation.html',
                        controller: 'message_consultationCtrl'
                    }
                }
            })

            .state('menu.messagetherapy_assissment', {
                url: '/messagetherapy/assissment',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/forms/message/assissment.html',
                        controller: 'message_assissmentCtrl'
                    }
                }
            })

            .state('menu.messagetherapy_feedback', {
                url: '/messagetherapy/feedback',
                cache: false,
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/forms/message/feedback.html',
                        controller: 'message_feedbackCtrl'
                    }
                }
            })

    });