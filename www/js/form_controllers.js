angular.module('app.form_controllers', [
        'app.services',
        'app.directives'
    ])


    /* S&C Cosultation controller
     * ***********************************************************************************
     */
    .controller('sncConsultationCtrl', function ($scope, $rootScope, $location, formService, utils, baseSetting) {

        $scope.init = function () {
            console.debug('controler');
            $scope.display = {
                'totalform': 5,
                'currentform': 1,
            };
            $scope.option = {};
            $scope.fields = {
                'registration_id': $rootScope.storage.tempselectuser.registration_id
            };
            // $scope.fields = {
            //     'patient_name': 'sd',
            //     'mobile_number': '999'
            // };

            var formOpObj = formService.getOption({
                'company_id': baseSetting.company_id,
                'form_id': 10
            });

            formOpObj.then(function (response) {
                if (response.data.status) {
                    $scope.option = {
                        'fitness_goal': response.data.options[0]['option_parse'],
                        'motivate_fitness': response.data.options[1]['option_parse'],
                        'often_exerise': response.data.options[2]['option_parse'],
                        'activities_prefer': response.data.options[3]['option_parse'],
                        'sleep_daily': response.data.options[4]['option_parse'],
                    };
                    console.debug($scope.option);
                } else {
                    $scope.option = {};
                }
                console.debug(response);
            }, function (response) {
                console.debug(response);
            });
        };

        $scope.nextform = function () {
            $scope.display.currentform = $scope.display.currentform + 1;
        };

        $scope.previousform = function () {
            $scope.display.currentform = $scope.display.currentform - 1;
        };

        $scope.submitConsultation = function () {

            var submitFeedback = formService.formSubmition($scope.fields, 'forms/snc/consultation_create');
            submitFeedback.then(function (res) {
                if (res.data.status) {
                    utils.alertMessage('Woooh! Success.', 'Form is submited.').then(function (res) {
                        $location.path('/side-menu21/customer-details/' + $rootScope.storage.tempselectuser.registration_id);
                    });
                } else {
                    utils.alertMessage('Error!', 'Please enter the required details properly.');
                }
            }, function (res) {
                console.debug(res);
            });
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    .controller('sncAssessmentCtrl', function ($scope, $rootScope, $location, formService, baseSetting) {
        $scope.init = function () {
            console.debug('snc assessment controller');
            console.debug('windows innerWidth is ', window.innerWidth);

            $scope.display = {
                'totalForm': 9,
                'currentForm': 1,
            };
            $scope.option = {};

            $scope.fields = {
                'first_name': 'Gaurish Rane',
                'mobile_no': 9850950086
            };

            //var formOpObj = formService.getOption({
            //    'company_id': baseSetting.company_id,
            //    'form_id': 10
            //});
            //
            //formOpObj.then(function (response) {
            //    if (response.data.status) {
            //        $scope.option = {};
            //        console.debug($scope.option);
            //    } else {
            //        $scope.option = {};
            //    }
            //    console.debug(response);
            //}, function (response) {
            //    console.debug(response);
            //});
        };

        $scope.nextForm = function () {
            $scope.display.currentForm = $scope.display.currentForm + 1;
        };

        $scope.previousForm = function () {
            $scope.display.currentForm = $scope.display.currentForm - 1;
        };

        $scope.save = function () {
            console.debug('save the snc consultaion', $scope.fields);
        };

        $scope.calculateBMI = function () {
            var ht_c = ($scope.fields.fit_height / 100);
            var bmi = ($scope.fields.fit_weight / (ht_c * ht_c));
            $scope.fields.fit_bmi = parseFloat(bmi.toFixed(2));
        };

        $scope.calculateBodyFat = function () {
            $scope.fields.body_total = ($scope.fields.body_triceps + $scope.fields.body_subscapula + $scope.fields.body_suprailiac +
            $scope.fields.body_Abdomen + $scope.fields.body_chest + $scope.fields.body_front_thigh);

            if ($scope.fields.body_total) {
                var square_of_skin_fold = $scope.fields.body_total * $scope.fields.body_total;
                var a = parseFloat((0.29288 * $scope.fields.body_total).toFixed(5));
                console.log('a=', a);
                var b = (0.0005 * square_of_skin_fold);
                console.log('b=', b);
                var c = (0.15845 * 25);
                console.log('c=', c);

                var bodyfat = a - b + c - 5.76377;
                $scope.fields.estimate_body_fat_per = parseFloat((bodyfat).toFixed(4));
                // var bodyfat = 1 - 5.76377;
                console.log('body fat cal', $scope.fields.body_total);
            } else {
                $scope.fields.estimate_body_fat_per = false;
            }
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    .controller('sncFeedbackCtrl', function ($scope, $rootScope, $location, ionicDatePicker, utils, formService, moment) {

        var ipObj1 = {
            callback: function (val) { //Mandatory
                console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                console.log('format data', moment(new Date(val)).format("DD/MMM/YYYY"));
            },
        };

        $scope.fields = {};

        $scope.init = function () {
            console.debug('controler');
            $scope.display = {
                'totalform': 4,
                'currentform': 1,
            };
        };

        $scope.showDate = function () {
            ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.submitFeedback = function () {
            console.debug($scope.fields);
            var submitFeedback = formService.formSubmition($scope.fields, 'forms/snc/feedbackcreate');
            submitFeedback.then(function (res) {
                if (res.data.status) {
                    utils.alertMessage('Woooh! Success.', 'Form is submited.').then(function (res) {
                        $location.path('/side-menu21/dashboard');
                    });
                } else {
                    utils.alertMessage('Error!', 'Please enter the required details properly.');
                }
            }, function (res) {
                console.debug(res);
            });
        };

        $scope.nextform = function () {
            $scope.display.currentform = $scope.display.currentform + 1;
        };

        $scope.previousform = function () {
            $scope.display.currentform = $scope.display.currentform - 1;
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    /* Physiotherapay Controller
     * ***********************************************************************************
     */
    .controller('physiotherpayConsultationCtrl', function ($scope, $rootScope, baseSetting, $location, utils, formService, moment, ionicDatePicker) {
        $scope.fields = {};
        $scope.init = function () {

            utils.showLoading();
            $scope.display = {
                'totalform': 2,
                'currentform': 1,
            };
            $scope.fields = {
                'registration_id': ($rootScope.storage.tempselectuser.registration_id) ? parseInt($rootScope.storage.tempselectuser.registration_id) : '',
                'custom_date': moment(new Date()).format("DD/MMM/YYYY")
            };

            //load the form options

            var formOpObj = formService.getOption({
                'company_id': baseSetting.company_id,
                'form_id': 7
            });

            formOpObj.then(function (response) {
                utils.hideLoading();
                if (response.data.status) {
                    $scope.option = {
                        'often_exerise': response.data.options[0]['option_parse'],
                        //'fitness_goal': response.data.options[0]['option_parse'],
                        //'motivate_fitness': response.data.options[1]['option_parse'],
                        //'activities_prefer': response.data.options[3]['option_parse'],
                    };
                    console.debug($scope.option);
                } else {
                    $scope.option = {};
                }
                console.debug(response);
            }, function (response) {
                utils.hideLoading();
                console.debug(response);
            });
        };


        $scope.nextform = function () {
            $scope.display.currentform = $scope.display.currentform + 1;
        };

        $scope.previousform = function () {
            $scope.display.currentform = $scope.display.currentform - 1;
        };

        $scope.submitConsulatation = function () {
            utils.showLoading();
            console.debug($scope.fields);
            var submitFeedback = formService.formSubmition($scope.fields, 'forms/physio/consultation_create');
            submitFeedback.then(function (res) {
                utils.hideLoading();
                if (res.data.status) {
                    utils.alertMessage('Woooh! Success.', 'Form is submited.').then(function (res) {
                        $location.path('/side-menu21/customer-details/' + $rootScope.storage.tempselectuser.registration_id);
                    });
                } else {
                    utils.alertMessage('Error!', 'Please enter the required details properly.');
                }
            }, function (res) {
                utils.hideLoading();
                console.debug(res);
            });
        };

        $scope.triggerDate = function () {
            var EndDateObj = {
                callback: function (val) { //Mandatory
                    console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                    $scope.fields.custom_date = moment(new Date(val)).format("DD/MMM/YYYY");
                },
            };
            ionicDatePicker.openDatePicker(EndDateObj);
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    .controller('physioAssessmentCtrl', function ($scope, $rootScope, $location, utils, formService) {

        $scope.init = function () {
            console.debug('controler');
            $scope.display = {
                'totalform': 3,
                'currentform': 1,
            };
            $scope.fields = {
                'registration_id': $rootScope.storage.tempselectuser.registration_id
            };
        };

        $scope.nextform = function () {
            $scope.display.currentform = $scope.display.currentform + 1;
        };

        $scope.previousform = function () {
            $scope.display.currentform = $scope.display.currentform - 1;
        };

        $scope.submitAssessment = function () {
            utils.showLoading();
            console.debug($scope.fields);
            var submitFeedback = formService.formSubmition($scope.fields, 'forms/physio/assessment_create');
            submitFeedback.then(function (res) {
                utils.hideLoading();
                if (res.data.status) {
                    utils.alertMessage('Woooh! Success.', 'Form is submit.').then(function (res) {
                        $location.path('/side-menu21/customer-details/' + $rootScope.storage.tempselectuser.registration_id);
                    });
                } else {
                    utils.alertMessage('Error!', 'Please enter the required details properly.');
                }
            }, function (res) {
                utils.hideLoading();
                console.debug(res);
            });
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    .controller('physiotherpayFeedbackCtrl', function ($scope, $rootScope, $location, utils, formService) {

        $scope.fields = {};
        $scope.init = function () {
            console.debug('controler');
            $scope.display = {
                'totalform': 3,
                'currentform': 1,
            };
            $scope.fields = {
                'first_name': ($rootScope.storage.tempselectuser.first_name) ? $rootScope.storage.tempselectuser.first_name : '',
                'last_name': ($rootScope.storage.tempselectuser.last_name) ? $rootScope.storage.tempselectuser.last_name : '',
                'mobile_no': ($rootScope.storage.tempselectuser.mobile_no) ? parseInt($rootScope.storage.tempselectuser.mobile_no) : '',
                'registration_id': ($rootScope.storage.tempselectuser.registration_id) ? parseInt($rootScope.storage.tempselectuser.registration_id) : '',
                // 'custom_date': moment(new Date()).format("DD/MMM/YYYY")
            };
        };

        $scope.submitFeedback = function () {
            utils.showLoading();
            var submitFeedback = formService.formSubmition($scope.fields, 'forms/physio/feedbackcreate');
            submitFeedback.then(function (res) {
                utils.hideLoading();
                if (res.data.status) {
                    utils.alertMessage('Woooh! Success.', 'Form is submited.').then(function (res) {
                        $location.path('/side-menu21/dashboard');
                    });
                } else {
                    utils.alertMessage('Error!', 'Please enter the required details properly.');
                }
            }, function (res) {
                utils.hideLoading();
                console.debug(res);
            });
        };

        $scope.nextform = function () {
            $scope.display.currentform = $scope.display.currentform + 1;
        };

        $scope.previousform = function () {
            $scope.display.currentform = $scope.display.currentform - 1;
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })


    .controller('physioCommentCtrl', function ($scope, $rootScope, $location, utils, formService) {

        $scope.fields = {
            'registration_id': $rootScope.storage.tempselectuser.registration_id
        };
        $scope.init = function () {
            console.debug('controler');
            $scope.display = {
                'totalform': 3,
                'currentform': 1,
            };
            $scope.fields = {
                'first_name': ($rootScope.storage.tempselectuser.first_name) ? $rootScope.storage.tempselectuser.first_name : '',
                'last_name': ($rootScope.storage.tempselectuser.last_name) ? $rootScope.storage.tempselectuser.last_name : '',
                'mobile_no': ($rootScope.storage.tempselectuser.mobile_no) ? parseInt($rootScope.storage.tempselectuser.mobile_no) : '',
                'registration_id': ($rootScope.storage.tempselectuser.registration_id) ? parseInt($rootScope.storage.tempselectuser.registration_id) : '',
                // 'custom_date': moment(new Date()).format("DD/MMM/YYYY")
            };
        };

        $scope.submitComment = function () {
            utils.showLoading();
            var submitFeedback = formService.formSubmition($scope.fields, 'forms/physio/comment_create');
            submitFeedback.then(function (res) {
                utils.hideLoading();
                if (res.data.status) {
                    utils.alertMessage('Woooh! Success.', 'Form is submited.').then(function (res) {
                        $location.path('/side-menu21/customer-details/' + $rootScope.storage.tempselectuser.registration_id);
                    });
                } else {
                    utils.alertMessage('Error!', 'Please enter the required details properly.');
                }
            }, function (res) {
                utils.hideLoading();
                console.debug(res);
            });
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })
    /* Message controller
     * ***********************************************************************************
     */

    .controller('message_consultationCtrl', function ($scope, $rootScope, $location, baseSetting, utils, formService, moment, ionicDatePicker) {
        $scope.option = {};
        $scope.fields = {};

        $scope.init = function () {
            console.debug('controler');
            $scope.display = {
                'totalform': 2,
                'currentform': 1,
            };
            $scope.fields = {
                'first_name': ($rootScope.storage.tempselectuser.first_name) ? $rootScope.storage.tempselectuser.first_name : '',
                'last_name': ($rootScope.storage.tempselectuser.last_name) ? $rootScope.storage.tempselectuser.last_name : '',
                'mobile_no': ($rootScope.storage.tempselectuser.mobile_no) ? parseInt($rootScope.storage.tempselectuser.mobile_no) : '',
                'registration_id': ($rootScope.storage.tempselectuser.registration_id) ? parseInt($rootScope.storage.tempselectuser.registration_id) : '',
                'custom_date': moment(new Date()).format("DD/MMM/YYYY")
            };
        };

        $scope.nextform = function () {
            $scope.display.currentform = $scope.display.currentform + 1;
        };

        $scope.previousform = function () {
            $scope.display.currentform = $scope.display.currentform - 1;
        };

        $scope.submitConsulatation = function () {
            console.debug($scope.fields);
            utils.showLoading();
            var submitFeedback = formService.formSubmition($scope.fields, 'forms/message/consultation_create');
            submitFeedback.then(function (res) {
                utils.hideLoading();
                if (res.data.status) {
                    utils.alertMessage('Woooh! Success.', 'Form is submited.').then(function (res) {
                        $location.path('/side-menu21/customer-details/' + $rootScope.storage.tempselectuser.registration_id);
                    });
                } else {
                    utils.alertMessage('Error!', 'Please enter the required details properly.');
                }
            }, function (res) {
                utils.hideLoading();
                console.debug(res);
            });
        };

        $scope.triggerDate = function () {
            var EndDateObj = {
                callback: function (val) { //Mandatory
                    console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                    $scope.fields.custom_date = moment(new Date(val)).format("DD/MMM/YYYY");
                },
            };
            ionicDatePicker.openDatePicker(EndDateObj);
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    // message assissment details not provided.
    //todo:: remove the message assissment controller
    .controller('message_assissmentCtrl', function ($scope, $rootScope, $location) {
        $scope.init = function () {
            console.debug('controler');
            $scope.display = {
                'totalform': 3,
                'currentform': 1,
            };
        };

        $scope.nextform = function () {
            $scope.display.currentform = $scope.display.currentform + 1;
        };

        $scope.previousform = function () {
            $scope.display.currentform = $scope.display.currentform - 1;
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    .controller('message_feedbackCtrl', function ($scope, $rootScope, $location, utils, formService) {
        $scope.fields = {};
        $scope.init = function () {
            console.debug('controler');
            $scope.display = {
                'totalform': 3,
                'currentform': 1,
            };
            $scope.fields = {
                'first_name': ($rootScope.storage.tempselectuser.first_name) ? $rootScope.storage.tempselectuser.first_name : '',
                'last_name': ($rootScope.storage.tempselectuser.last_name) ? $rootScope.storage.tempselectuser.last_name : '',
                'mobile_no': ($rootScope.storage.tempselectuser.mobile_no) ? parseInt($rootScope.storage.tempselectuser.mobile_no) : '',
                'registration_id': ($rootScope.storage.tempselectuser.registration_id) ? parseInt($rootScope.storage.tempselectuser.registration_id) : '',
                // 'custom_date': moment(new Date()).format("DD/MMM/YYYY")
            };
        };

        $scope.submitFeedback = function () {
            var submitFeedback = formService.formSubmition($scope.fields, 'forms/message/feedbackcreate');
            submitFeedback.then(function (res) {
                if (res.data.status) {
                    utils.alertMessage('Woooh! Success.', 'Form is submited.').then(function (res) {
                        $location.path('/side-menu21/dashboard');
                    });
                } else {
                    utils.alertMessage('Error!', 'Please enter the required details properly.');
                }
            }, function (res) {
                console.debug(res);
            });
        };

        $scope.nextform = function () {
            $scope.display.currentform = $scope.display.currentform + 1;
        };

        $scope.previousform = function () {
            $scope.display.currentform = $scope.display.currentform - 1;
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    });