"use strict";

angular.module('App.filters', []).filter('searchFilter', function () {
    return function (n, h) {
        h = h || '';
        h = h.toString();
        n = n.toString();
        var r = new RegExp(h, 'gi');
        return n.replace(r, '<span class="y">' + h + '</span>');
    };
}).filter('hilightFilter', function () {
    return function (number, hilight) {
        hilight = hilight || '';
        var h = hilight.toString().split('');
        var n = number.toString().split('');
        //console.log(h, n);
        angular.forEach(n, function (num, i) {
            if (h.length == 0)
                n[i] = '<span>' + n[i] + '</span>';
            else if (h.indexOf(i.toString()) != -1)
                n[i] = '<span class="y">' + n[i] + '</span>';
            else
                n[i] = '<span class="n">' + n[i] + '</span>';
        });
        return n.join('');
    };
}).filter('capitalizeF', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});