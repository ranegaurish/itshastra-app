angular.module('app', [
        'ionic',
        'ionic-material',
        'app.controllers',
        'app.form_controllers',
        'app.form_routes',
        'app.routes',
        'app.services',
        'app.directives',
        'ngStorage',
        'angularMoment',
        'App.filters',
        'ionic-datepicker',
        'ionic-toast',
        'ngCordova'
    ])
    .constant('baseSetting', {
        //'baseUrl': 'http://localhost/itshastra/external/',
        //'base': 'http://localhost/itshastra/',
        //'baseUrl': 'http://itshastra.in/heal/external/',
        //'base': 'http://itshastra.in/heal/',
        'baseUrl': 'http://dev.numberbazaar.com/external/',
        'baseUrl2': 'http://dev.numberbazaar.com/external2/',
        'base': 'http://dev.numberbazaar.com/',
        'company_id': '1',
        'company_name': 'Heal',

    })
    .config(function ($ionicConfigProvider, $httpProvider, $cordovaAppRateProvider) {
        // $ionicConfigProvider.scrolling.jsScrolling(false);
        $httpProvider.defaults.timeout = 10000; // 10 seconds timeout if the http does not get a response.
    })
    .run(function ($cordovaSplashscreen, $cordovaDevice, $rootScope, $timeout, $location, $ionicPlatform, $http, $window, $log, $localStorage, baseSetting, $q, utils) {
        $ionicPlatform.ready(function () {
            $rootScope.$location = $location;
            $rootScope.$watch('$location.path()', function (locationPath) {
                $rootScope.$broadcast('locationChange');
            });

            try {
                $cordovaSplashscreen.hide();
            } catch (e) {
            }

            var device;
            try {
                device = $cordovaDevice.getDevice();
            } catch (e) {
                device = {};
            }

            var m = device.manufacturer || 'Unknown device';
            var mo = device.model || '';
            $rootScope.device_name = m.substr(0, 1).toUpperCase() + m.substr(1, mo.length) + ' ' + mo;

            if (window.StatusBar)
                StatusBar.styleDefault();

            var storage = $rootScope.storage = $localStorage;

            function loadSetting() {
                console.debug('loading setting ...............');
                //$http.jsonp(baseSetting.baseUrl + 'appinit.jsonp?callback=JSON_CALLBACK', {
                //    params: {
                //        'company_id': baseSetting.company_id
                //    }
                //}).then(function (response) {
                //    if (response.data.status) {
                //        $rootScope.storage.appinit = {
                //            'companyDetails': response.data.data
                //        };
                //    } else {
                //        delete $rootScope.storage.appinit;
                //    }
                //}, function (response) {
                //    console.error(response);
                //})
            }

            // $rootScope.isOnline = false;
            // $rootScope.$on('$cordovaNetwork:online', function() {
            //     $rootScope.isOnline = true;
            //     $rootScope.$broadcast('network-change', $rootScope.isOnline);
            // });
            // $rootScope.$on('$cordovaNetwork:offline', function() {
            //     $rootScope.isOnline = false;
            //     $rootScope.$broadcast('network-change', $rootScope.isOnline);
            // });

            function appInitReady() {
                if (window.appReady)
                    return false; // app is already ready

                loadSetting();
                $rootScope.hideLoading = true;
                console.log('!!app ready!!');
                $rootScope.$broadcast('appReady');
                window.appReady = true;
            }


            function appFailure() { // this must never happen
                $rootScope.hideLoading = true;
                $log.log('app ready failure');
                window.appReady = false;
                $rootScope.error = 'Sorry, it seems like you\'re not connected to the internet.';
            }

            appInitReady();
            $rootScope.window = $window;
        });
    });