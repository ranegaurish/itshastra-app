angular.module('app.services', ['ngStorage'])

    .factory('someFactory', function () {

        var initFun = {};

        initFun.test = function () {

            return 'test_1';
        };

        return initFun;
    })


    /**
     * User Details Services
     */
    .service('userService', function ($rootScope, $log, baseSetting, $http, $q) {

        var that = this;
        this.loginCheck = function () {
            if ($rootScope.storage.userDetails) {
                return $rootScope.storage.userDetails;
            } else {
                return false;
            }
        };

        that.check_api = function (params) {
            var apiCall = $q.defer();

            $http.jsonp(baseSetting.baseUrl + 'login/check.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

        that.login = function (params) {
            var apiCall = $q.defer();

            $http.jsonp(baseSetting.baseUrl2 + 'login.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                if (response.data.status) {
                    $rootScope.storage.user = {
                        'details': response.data.userDetails,
                        'role': response.data.role,
                    };

                    $rootScope.storage.token = response.data.token;
                }
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

        //log out
        that.logout = function () {
            var apiCall = $q.defer();

            $http.jsonp(baseSetting.baseUrl + 'login/logout.jsonp?callback=JSON_CALLBACK', {
                params: {'token': $rootScope.storage.token}
            }).then(function (response) {
                delete $rootScope.storage.user;
                delete $rootScope.storage.token;
                delete $rootScope.storage.tempselectuser;
                // if (response.data.status) {
                // }
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

        //registration api
        that.registration = function (params) {
            var apiCall = $q.defer();

            $http.jsonp(baseSetting.baseUrl + 'formoption/filloptions.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

        //user registration search
        that.registrationSearch = function (params) {
            var apiCall = $q.defer();

            $http.jsonp(baseSetting.baseUrl + 'registration/search.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

        //user package search
        that.packageSearch = function (params) {
            var apiCall = $q.defer();

            $http.jsonp(baseSetting.baseUrl + 'package/search.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

        //entire customer details
        that.customerDetailsSearch = function (params) {
            var apiCall = $q.defer();
            var params = params;
            params.token = ($rootScope.storage.token) ? $rootScope.storage.token : null;

            $http.jsonp(baseSetting.baseUrl2 + 'customer.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

        //create procedure log
        that.createProcedureLog = function (params) {
            var apiCall = $q.defer();

            $http.jsonp(baseSetting.baseUrl + 'customer/procedure_create.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

        //create appointment
        that.createAppointment = function (params) {
            var apiCall = $q.defer();
            var params = params;
            params.token = ($rootScope.storage.token) ? $rootScope.storage.token : null;;

            $http.jsonp(baseSetting.baseUrl2 + 'customer/create_appointment.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

    })

    .service('formService', function ($localStorage, $rootScope, $q, $http, baseSetting) {
        var that = this;

        /*
         * Package
         *******************************************************************************************
         **/
        that.packageCreate = function (params) {
            var apiCall = $q.defer();
            $http.jsonp(baseSetting.baseUrl + 'package/create.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        }

        that.packageGet = function (params) {
            var apiCall = $q.defer();
            $http.jsonp(baseSetting.baseUrl + 'package/by_id.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        }

        that.getOption = function (params) {
            var apiCall = $q.defer();
            $http.jsonp(baseSetting.baseUrl + 'formoption/filloptions.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        }

        /*
         * Form submition form
         *******************************************************************************************
         **/
        that.formSubmition = function (params, url) {

            var apiCall = $q.defer();
            var params = params;
            params.token = ($rootScope.storage.token) ? $rootScope.storage.token : null;
            $http.jsonp(baseSetting.baseUrl2 + url + '.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        }

    })

    .service('settingLoader', function ($localStorage, $q, $http, baseSetting) {

        var that = this;

        that.homeSetting = function (params) {
            var apiCall = $q.defer();
            $http.jsonp(baseSetting.baseUrl + 'setting/home.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

        that.packageloader = function (params) {
            var apiCall = $q.defer();
            $http.jsonp(baseSetting.baseUrl + 'package/filloptions.jsonp?callback=JSON_CALLBACK', {
                params: params
            }).then(function (response) {
                apiCall.resolve(response);
            }, function (response) {
                apiCall.reject(response);
            })
            return apiCall.promise;
        };

        that.formSetting = function () {

        };

    })


    .service('utils', function ($rootScope, $ionicLoading, $ionicPopup) {

        var that = this;

        that.showLoading = function () {
            $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>',
            });
            return true;
        };

        that.alertMessage = function (title, message) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: message
            });

            return alertPopup;
        };

        that.hideLoading = function () {
            $ionicLoading.hide();
            return true;
        };

        /**
         * Arrays, Even objects. lol
         * @param array
         * @param chunkSize
         * @returns {Array.<T>}
         */
        this.chunk_array = function (array, chunkSize) {
            if (typeof array == 'object') {
                // wrap into array and do it.
                var a = [];
                angular.forEach(array, function (b, id) {
                    var e = {};
                    e[id] = b;
                    a.push(e);
                });
                array = a;
                console.log(array);
            }

            return [].concat.apply([],
                array.map(function (elem, i) {
                    return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
                })
            );
        };


        this.isEmpty = function (a) {
            return jQuery.isEmptyObject(a);
        };


    });