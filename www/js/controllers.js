angular.module('app.controllers', [
        'app.services',
        'app.directives'
    ])

    /* Login controller
     * ***********************************************************************************
     */
    .controller('loginCtrl', function ($scope, $rootScope, $location, userService, utils) {
        $scope.init = function () {
            console.debug('login ');
            $scope.loginData = {};
            if ($rootScope.storage.token) {
                $location.path('side-menu21/dashboard');
            }
        };

        $scope.login = function () {
            utils.showLoading();
            var userObj = userService.login($scope.loginData);

            userObj.then(function (response) {
                utils.hideLoading();
                if (response.data.status) {
                    $scope.loginError = '';
                    $location.path('side-menu21/dashboard');
                } else {
                    $scope.loginError = 'Invalid username and password.';
                }
            }, function (response) {
                utils.hideLoading();
                console.debug(response);
            });

        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()

    })

    /* Dashboard controller
     * ***********************************************************************************
     */
    .controller('dashboardCtrl', function ($scope, $rootScope, $location, userService, utils, formService) {

        $scope.init = function () {
            $scope.display = {
                'customerList': false,
                'formList': false
            };
            $scope.fields = {};
            utils.showLoading();
            var loadDashboard = formService.formSubmition({}, 'appointment/today');

            loadDashboard.then(function (res) {
                utils.hideLoading();
                if (res.data.status) {
                    $scope.appointment = res.data.appointment;
                } else {

                }
            }, function (res) {
                utils.hideLoading();
            });
        };

        $scope.registrationSearch = function () {
            $scope.display.customerList = true;
            utils.showLoading();
            var userObj = userService.registrationSearch($scope.fields);

            userObj.then(function (response) {
                utils.hideLoading();
                if (response.data.status) {
                    $scope.registrationList = response.data.data;
                } else {
                    $scope.registrationList = false;
                }
            }, function (response) {
                utils.hideLoading();
                $scope.registrationList = false;
            });
        };

        $scope.navCustomerDetail = function (userData) {
            $scope.selecteduser = $rootScope.storage.tempselectuser = userData;
            $location.path('side-menu21/customer-details/' + userData.registration_id);
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    /* Home controller
     * ***********************************************************************************
     */
    .controller('homeCtrl', function ($scope, $rootScope, $location, userService, utils) {

        $scope.init = function () {
            console.debug('home page debug');
            $scope.loginData = {};
        };

        $scope.login = function () {
            utils.showLoading();
            var userObj = userService.login($scope.loginData);

            userObj.then(function (response) {
                utils.hideLoading();
                if (response.data.status) {
                    $scope.loginError = '';
                    $location.path('side-menu21/dashboard');
                } else {
                    $scope.loginError = 'Invalid username and password.';
                }
            }, function (response) {
                utils.hideLoading();
                console.debug(response);
            });

        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })



    /* Customer Details controller
     * ***********************************************************************************
     */
    .controller('customerDetailsCtrl', function ($scope, $rootScope, $stateParams, $location, utils, userService) {


        $scope.init = function () {
            utils.showLoading();
            $scope.flags = {
                service_selection: true,
            };
            $scope.fields = {
                'registration_id': $stateParams.id,
                'token': ($rootScope.storage.token) ? $rootScope.storage.token : null,
            };

            var userObj = userService.customerDetailsSearch($scope.fields);

            userObj.then(function (response) {
                utils.hideLoading();
                if (response.data.status) {
                    $scope.flags.service_selection = false;
                    $scope.procedure_log = response.data.procedure;
                    $scope.appointment = response.data.appointment;
                    $scope.allocation = response.data.allocated;
                    $scope.service = response.data.service;
                    $scope.snc = response.data.snc;
                    $scope.physio = response.data.physio;
                    $rootScope.storage.appinit.formList = response.data.form_list;
                }
            }, function (response) {
                utils.hideLoading();

            });
        };


        $scope.select_procedure = function (form_details) {
            $location.path('side-menu21' + form_details.url);
        };

        //filter
        $scope.formTypeFilter = function (data) {
            return data.form_type_id == 2 || data.form_type_id == 5;
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })


    /* Appointment Create controller
     * ***********************************************************************************
     */
    .controller('appointmentCreateCtl', function ($scope, $rootScope, $location, ionicDatePicker, utils, userService) {

        var datepickObj = {
            callback: function (val) { //Mandatory
                console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                $scope.fields.appointment_date = moment(new Date(val)).format("DD/MMM/YYYY");
            },
        };

        $scope.init = function () {
            $scope.fields = {
                'registration_id': $rootScope.storage.tempselectuser.registration_id
            };


        };

        $scope.createAllocation = function () {
            utils.showLoading();
            var createAllocationObj = userService.createAppointment($scope.fields);

            createAllocationObj.then(function (response) {
                utils.hideLoading();
                if (response.data.status) {
                    utils.alertMessage('Success', 'Appointment created.').then(function (res) {
                        $location.path('side-menu21/customer-details/' + $rootScope.storage.tempselectuser.registration_id);
                    })
                } else {
                    utils.alertMessage('Error', 'Some thing when wrong.').then(function (res) {
                        $location.path('side-menu21/customer-details/' + $rootScope.storage.tempselectuser.registration_id);
                    })
                }
            }, function (response) {
                utils.hideLoading();
            });
        };

        $scope.trigger_datepicker = function () {
            $scope.fields.appointment_date = ionicDatePicker.openDatePicker(datepickObj);
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    /**
     * allocation
     */
    .controller('allocationCreateCtl', function ($scope, $rootScope, $location, ionicDatePicker, utils, userService) {

        var datepickObj = {
            callback: function (val) { //Mandatory
                console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                $scope.fields.appointment_date = moment(new Date(val)).format("DD/MMM/YYYY");
            },
        };

        $scope.init = function () {
            $scope.fields = {
                'registration_id': $rootScope.storage.tempselectuser.registration_id
            };


        };

        $scope.createAllocation = function () {
            utils.showLoading();
            var createAllocationObj = userService.createAppointment($scope.fields);

            createAllocationObj.then(function (response) {
                utils.hideLoading();
                if (response.data.status) {
                    utils.alertMessage('Success', 'Appointment created.').then(function (res) {
                        $location.path('side-menu21/customer-details/' + $rootScope.storage.tempselectuser.registration_id);
                    })
                } else {
                    utils.alertMessage('Error', 'Some thing when wrong.').then(function (res) {
                        $location.path('side-menu21/customer-details/' + $rootScope.storage.tempselectuser.registration_id);
                    })
                }
            }, function (response) {
                utils.hideLoading();
            });
        };

        $scope.trigger_datepicker = function () {
            $scope.fields.appointment_date = ionicDatePicker.openDatePicker(datepickObj);
        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    /* Location controller
     * ***********************************************************************************
     */
    .controller('locationCtrl', function ($scope, $rootScope) {

        $scope.init = function () {

        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    /* Contact controller
     * ***********************************************************************************
     */
    .controller('contactCtrl', function ($scope, $rootScope) {

        $scope.init = function () {

        };

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })

    /* Registration controller
     * ***********************************************************************************
     */
    .controller('registrationCtrl', function ($scope, $rootScope, $http, $location, baseSetting, moment, userService, utils, $rootScope, ionicDatePicker) {

        var datepickObj = {
            callback: function (val) { //Mandatory
                console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                $scope.fields.date_of_birth = moment(new Date(val)).format("DD/MMM/YYYY");
            },
        };

        $scope.init = function () {
            $scope.display = {
                'totalform': 3,
                'currentform': 1,
            };
            $scope.fields = {
                'company_id': baseSetting.company_id
            };

            //get the form set the options
            var userObj = userService.registration({
                'company_id': baseSetting.company_id,
                'form_id': 13
            });

            userObj.then(function (response) {
                if (response.data.status) {
                    $scope.formOptions = {
                        designation: response.data.options[0]['option_parse'],
                        referred: response.data.options[1]['option_parse'],
                        hear_about_us: response.data.options[2]['option_parse']
                    };
                } else {
                    $scope.formOptions = false;
                }
            }, function (response) {
                console.error(response);
            });
        };

        $scope.trigger_datepicker = function () {
            $scope.fields.date_of_birth = ionicDatePicker.openDatePicker(datepickObj);
        };

        $scope.nextform = function () {
            console.debug('data ', $scope.fields);
            $scope.display.currentform = $scope.display.currentform + 1;
        };

        $scope.previousform = function () {
            console.debug('data ', $scope.fields);
            $scope.display.currentform = $scope.display.currentform - 1;
        };

        //submit the register customer
        $scope.submitRegister = function () {
            console.debug('data ', $scope.fields);
            $http.jsonp(baseSetting.baseUrl + 'registration/create.jsonp?callback=JSON_CALLBACK', {
                params: $scope.fields
            }).then(function (response) {
                if (response.data.status) {
                    $scope.fields.registration_id = response.data.data;
                    $rootScope.storage.tempselectuser = $scope.fields;
                    console.log($rootScope.storage.tempselectuser);
                    utils.alertMessage('Woooh! Success.', 'You have successfully register a new customer.').then(function (res) {
                        $location.path('/side-menu21/customer-details/' + response.data.data);
                    });
                } else {
                    utils.alertMessage('Invalid value', 'Please enter all required fields propely.').then(function (res) {

                    });
                }
            }, function (response) {
                console.error(response);
            })
        }

        $scope.$on('appReady', function () {
            $scope.init()
        })

        if (typeof appReady != 'undefined' && appReady)
            $scope.init()
    })


    .controller('logoutCtrl', function ($rootScope, $log, $location) {
        // $scope.logout = function () {
        //     utilService.showloading();
        //     userService.logout().then(function (response) {
        //         utilService.hideloading()
        //         $log.info(response)
        //         if (response.data.status) {
        //             $location.path('side-menu21/home')
        //         }
        //     }, function (response) {
        //         $log.error('controller...', response)
        //         $rootScope.connectionLost($scope.logout);
        //     });
        // }
        // $scope.logout();
    })


    .controller('connectionCtrl', function ($scope, $location) {
        console.log('connection lost');
        // $scope.try_again = function () {
        //     var check = checkFactory.checkConnection({ back_home: true })
        // };
        //             $scope.try_again()
    })

    .controller('menuCtrl', function ($location, $cordovaToast, $scope, userService, $ionicSideMenuDelegate) {
        // $scope.showlogin = function () {
        //     var temp = utilService.login_model($scope)
        // };

        $scope.showlogout = function () {
            console.log('logout trigger');

            $scope.close_menu();
            userService.logout().then(function (response) {
                $location.path('/side-menu21/login');
                $cordovaToast.showLongBottom('You\'ve been logged out.');
                // if (response.data.status) {
                // }
            }, function (response) {

            })
        };

        $scope.close_menu = function () {
            $ionicSideMenuDelegate.toggleLeft()
        };
        $scope.goto = function (url) {
            $location.path('/side-menu21' + url);
        };
    })

    .controller('testCtrl', function ($scope, $rootScope) {
        console.log('logout trigger');
    });